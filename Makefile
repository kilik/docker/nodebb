.PHONY: help

## Display this help text
help:
	$(info ---------------------------------------------------------------------)
	$(info -                        Available targets                          -)
	$(info ---------------------------------------------------------------------)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                                \
	nb = sub( /^## /, "", helpMsg );                                            \
	if(nb == 0) {                                                               \
		helpMsg = $$0;                                                      \
		nb = sub( /^[^:]*:.* ## /, "", helpMsg );                           \
	}                                                                           \
	if (nb)                                                                     \
		printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg;          \
	}                                                                           \
	{ helpMsg = $$0 }'                                                          \
	$(MAKEFILE_LIST) | column -ts:

# auto configure .env
.env: .env.dist
	cp .env.dist .env

# auto configure config.json
config.json: config.json.dist
	cp config.json.dist config.json

# auto configure config.json
package.json: package.json.dist
	cp package.json.dist package.json

## autoconfig
autoconfig: .env config.json package.json

## pull last dependencies
pull: autoconfig
	docker-compose pull

## start the service
start: autoconfig
	docker-compose up -d

## up (start alias)
up: start

## stop the service
stop:
	docker-compose stop

# stop only nodebb
stop-nodebb:
	docker-compose stop nodebb

## upgrade
upgrade: stop-nodebb internal_upgrade start

internal_upgrade:
	docker-compose run --rm nodebb ./nodebb upgrade

## remove the service
down:
	docker-compose down --remove-orphans --volumes

## restart the proxy service
restart: stop start

## enter in nodebb container
nodebb:
	docker-compose exec nodebb bash

## show logs
logs:
	docker-compose logs -f --tail=1000

## show status
ps:
	docker-compose ps

## setup (install)
setup: pull stop internal_setup up

# internal setup (install)
internal_setup:
	docker-compose run --rm nodebb ./nodebb setup

# internal build
internal_build:
	docker-compose run --rm nodebb ./nodebb build

## backup data (compress)
backup:
	./backup.sh
